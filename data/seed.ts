import { UserToCreateDto } from "./models";
import prisma from "./prismaClient";

async function seed() {
    const newUsers: string[][] = [
        ['Peppa', 'Pig', 'peppa@gmail.com'],
        ['Suzie', 'Sheep', 'suzie@hotmail.com'],
        ['Danny', 'Dog', 'danny@yahoo.com'],
        ['Pedro', 'Pony', 'pedro@outlook.com'],
        ['Rebecca', 'Rabbit', 'rebecca@spark.com'],
        ['Emily', 'Elephant', 'emily@aol.com'],
    ];

    await prisma.user.createMany({
        data: newUsers.map(i => <UserToCreateDto>{
            firstName: i[0],
            lastName: i[1],
            email: i[2]
        })
    })
}

seed();
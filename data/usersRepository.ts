import prisma from "./prismaClient";
import { Pagination, UserToCreateDto, UserToGet, UserToUpdateDto } from './models';

export const searchUsers = async (where: object, { take, skip }: Pagination, select?: object): Promise<any[]> =>
    await prisma.user.fetchPaginated(where, { take, skip }, select)

export const addUser = async (newUser: UserToCreateDto) =>  
    await prisma.user.create({ data: {...newUser} });

export const updateUser = async (userToUpdate: UserToUpdateDto, id: number) =>  
    await prisma.user.update({ data: {...userToUpdate}, where: {id} });

export const deleteUser = async (id: number) =>  
    await prisma.user.delete({ where: {id} });

export const getUser = async (id: number): Promise<UserToGet | null> =>
    await prisma.user.findFirst({where: {id}})

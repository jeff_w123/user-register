import prisma from "../data/prismaClient";

import { UserToGet } from "../data/models";

/**
 * Helper functions for the tests
 */
export const addUsers = async () => {
    const newUsers: string[][] = [
        ['Peppa', 'Pig', 'peppa@gmail.com'],
        ['Suzie', 'Sheep', 'suzie@hotmail.com'],
        ['Danny', 'Dog', 'danny@yahoo.com'],
        ['Pedro', 'Pony', 'pedro@outlook.com'],
        ['Rebecca', 'Rabbit', 'rebecca@spark.com'],
        ['Emily', 'Elephant', 'emily@aol.com'],
    ];

    await prisma.user.createMany({
        data: newUsers.map(i => <UserToGet>{
            firstName: i[0],
            lastName: i[1],
            email: i[2]
        })
    })
}

export const addUser = async (email: string) => {
    await prisma.user.create({
        data: <UserToGet>{
            firstName: 'Harry',
            lastName: 'Hamster',
            email
        }
    })
}

export const deleteAllUsers = async () => await prisma.user.deleteMany();
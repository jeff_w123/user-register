import { PrismaClientKnownRequestError } from "@prisma/client/runtime/library";
import { Response as ExResponse, Request as ExRequest, NextFunction } from "express";
import { ValidateError } from "tsoa";

/**
 * A middleware handler to return errors in a common format
 * @param err
 * @param req 
 * @param res 
 * @param next 
 * @returns 
 */
export const errorHandler = (
    err: unknown,
    req: ExRequest,
    res: ExResponse,
    next: NextFunction
  ): ExResponse | void  => {

  // input validation errors
  if (err instanceof ValidateError) {
    return res.status(422).json({
      message: "Validation Failed",
      details: { ...err.fields },
    });
  }

  // user errors (database constraint, or record not found)
  if (err instanceof PrismaClientKnownRequestError && ['P2002', 'P2025'].includes(err.code))
    return res.status(500).json({
      message: "There was an error",
      details: {
        serverError: { message: 'Try again, or check docs if the problem persists.'}},
    });  

  // unhandled
  if (err instanceof Error) {
    console.log(err) // ==> send to Sentry
    return res.status(500).json({
      message: "There was an error on our server",
      details: {
        serverError: { message: 'So sorry about that! We have been alerted to the issue. Try again, or get in touch if the problem persists.'}},
    });  
  }

  next();
};
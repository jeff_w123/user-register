# User Register

## Task spec
<details>
  <summary>Details</summary>
  
### Back-end Technical Exercise

#### Overview
A new user register needs to be established as part of designing a new electric system. This exercise covers building the services to support this new register.
  
##### Goal
Design and implement a RESTful API to retrieve user information.

#### Features
* The API must have the ability for a client to retrieve users matching a requested surname
* Each user’s details are to contain the following information:
    * Surname
    * First name
    * Email Address

#### Technical Requirements
* The service is to be implemented:

    * Using Spring Boot or Micronaut with Kotlin or Java. Other
dependencies/libraries can be included to provide the required functionality
    * Or Using Nodejs in typescript
* User data can be pre-populated, or a RESTful endpoint exposed to add users to the
system
* Data can be stored in memory or a database
* The application code should contain at least one test
* Include a README file with instructions on how to build, run and use the application

#### What To Submit
* Please submit a link to a git repository containing the application's code.

#### Timeframe
* The recommended time to spend on this task is a couple of hours.
<br>
<hr>
<br>

<br>

</details>

## Requirements / tested on
Docker version 26.0.0
<br>
Docker Compose version v2.26.1
<br>
Node v20.11.1
<br>
Host machine requires a free tcp port (default 3000) or optionally, set API_PORT in .env [here](.env).
<br>

# Setup
Clone this repo, then terminal to project root.
<br>

# Api

## Start api
```sh
docker-compose -f docker-compose.run.yml up -d
```
Wait for image builds, container starts, + ~ 1 minute (for api initialization)


## Browse api
Open api-docs (Swagger url below)
<br>
<br>
If NOT using default port 3000, then change the url port as needed.
<br>

```sh
http://localhost:3000/api-docs
```

## Stop api, remove resources

```sh
docker-compose -f docker-compose.run.yml down --rmi all
```

# Tests

## Run / view tests in terminal

The first execution is not cached. It will take 3 to 4 minutes to build and run to completion.
```sh
docker-compose -f docker-compose.test.yml up
```

## Exit / remove resources
```sh
docker-compose -f docker-compose.test.yml down --rmi all
```
## Developer Installation
<details>
  <summary>Details</summary>

## Requirements / tested on
PostgreSQL 16.2
<br>
Node v20.11.1
<br>
Developer machine requires a free tcp port (default 3000) or optionally, set API_PORT in .env [here](.env).
<br>

## Install and Build
```sh
npm i
```
Run migrations and build prisma client
<br>
```sh
prisma migrate dev
```
Seed db (optional)
<br>
```sh
prisma db seed
```
Build routes, documentation and compile ts to dist
<br>
```sh
npm run build
```
Rebuild api routes and docs (during dev, for changes to routes)
```sh
tsoa spec-and-docs
```

## Start api
Start with or without watcher
<br>
```sh
npm run dev
```
or
```sh
npm run start
```

## Run tests
```sh
npm run test
```

</details>
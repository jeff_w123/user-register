FROM node:21-alpine

# Install curl
RUN apk add --no-cache curl

WORKDIR /usr/app
COPY package.json .
RUN npm install
COPY . .

EXPOSE ${API_PORT}
CMD ["npm", "run", "start"]

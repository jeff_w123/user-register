import {
    Body,
    Controller,
    Get,
    Post,
    Query,
    Route,
    SuccessResponse,
    Request,
    Response,
    Path,
    Put,
    Delete,
  } from "tsoa";
import express from "express";

import { PageDto, PageSizeDto, UserToGet, UserToCreateDto, CreatedId, UserToUpdateDto } from "../data/models";
import createUser from "./createUser"
import getUsers from "./getUsers"
import getUser from "./getUser"
import { getPaginationHeaders, getPaginationInfo } from "../utilities/common";
import updateUser from "./updateUser";
import deleteUser from "./deleteUser";


@Route("v1/users")
export class V1UsersController extends Controller {

  /**
  * Creates a new user.
  * Supply a unique email address, first name and last name.
  * 
  */
  @Post()
  @SuccessResponse(201, 'Created')
  @Response(422, 'Validation Failed')
  public async createUser( @Body() userToCreate: UserToCreateDto): Promise<CreatedId> {
    const createdId = await createUser(userToCreate);
    this.setStatus(201);
    return createdId;
  }

  /**
  * Retrieves a list of users.
  * Supply optional pagination and search parameters to receive the corresponding result.
  * 
  */
  @SuccessResponse("200", "Get successful")
  @Response(422, 'Validation Failed')
  @Get()
  public async getUsers(
    @Request() req: express.Request,
    @Query() page?: PageDto,
    @Query() pageSize?: PageSizeDto,
    @Query() lastName?: string,
  ): Promise<UserToGet[]> {

    const {take, skip} = getPaginationInfo(page, pageSize);

    const {results, count} = await getUsers({ take, skip}, lastName);
    
    // add pagination links to headers
    getPaginationHeaders(take, skip, count, req.url)
      .forEach(i => {this.setHeader(i[0], i[1])});

    this.setStatus(200);
    return results;
  }
  
  /**
  * Retrieves a user by id.
  * Supply user id to receive the corresponding result.
  * 
  */
  @SuccessResponse("200", "Get successful")
  @Get("{userId}")
  public async getUser(
    @Path() userId: number,
  ): Promise<UserToGet | null> {
    const foundUser = await getUser(userId);
    this.setStatus(200);
    return foundUser;
  }

  /**
  * Update a user.
  * Supply a user id.
  * 
  */
  @Put("{userId}")
  @SuccessResponse(204, 'Updated')
  public async updateUser( 
    @Path() userId: number, 
    @Body() userToUpdate: UserToUpdateDto
  ): Promise<void> {
    await updateUser(userToUpdate, userId);
    this.setStatus(204);
    return;
  }

  /**
  * Delete a user.
  * Supply a user id.
  * 
  */
  @Delete("{userId}")
  @SuccessResponse(204, 'Deleted')
  public async deleteUser( 
    @Path() userId: number
  ): Promise<void> {
    await deleteUser(userId);
    this.setStatus(204);
    return;
  }

}